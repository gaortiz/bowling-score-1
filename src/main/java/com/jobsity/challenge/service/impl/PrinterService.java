package com.jobsity.challenge.service.impl;

import com.jobsity.challenge.model.FramesResult;
import com.jobsity.challenge.service.IPrinterService;

import java.util.List;
import java.util.Map;

public class PrinterService implements IPrinterService {

    public void printResults(List<FramesResult> framesResults) {
        System.out.println("\nFrame\t\t1\t\t2\t\t3\t\t4\t\t5\t\t6\t\t7\t\t8\t\t9\t\t10");
        framesResults.forEach(framesResult -> {
            final List<Integer> score = framesResult.getScore();
            final Map<Integer, List<String>> pinFalls = framesResult.getPinFalls();
            final String player = framesResult.getPlayer();
            System.out.println(player);
            printPinFalls(pinFalls);
            printScore(score);
            System.out.print("\n");
        });
    }

    private void printPinFalls(Map<Integer, List<String>> pinFalls) {
        System.out.print("Pinfalls\t");
        pinFalls.values().forEach(pinFall -> {
            final String pinFallString = pinFall.toString().replace("[", "")
                    .replace("]", "")
                    .replace(",", "\t");
            if (pinFall.size() == 1) {
                System.out.print("\t" + pinFallString + "\t");
            } else {
                System.out.print(pinFallString + "\t");
            }
        });
    }

    private void printScore(List<Integer> score) {
        System.out.print("\nScore\t\t");
        score.forEach(value -> System.out.print(value + "\t\t"));
    }

}
