package com.jobsity.challenge.exceptions;

public class InvalidScoreException extends BowlingScoreException {

    public InvalidScoreException() {
        super("Shot values should be numbers from 0 to 10 or F letter");
    }

}
